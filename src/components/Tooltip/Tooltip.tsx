import { JSX, useState } from "react";
import { Info } from "lucide-react";

export interface TooltipProps {
    type?: "sm" | "md" | "lg";
    className?: string;
    icons?: { icon: JSX.Element; label: string }[];
}

const Tooltip: React.FC<TooltipProps> = ({ type = "md", className = "", icons = [] }) => {
    const [open, setOpen] = useState(false);

    const typeClasses = {
        sm: "h-6 w-6",
        md: "h-8 w-8",
        lg: "h-12 w-12"
    };

    /* const iconSize = {
        sm: "h-4 w-4",
        md: "h-6 w-6",
        lg: "h-8 w-8"
    }; */

    return (
        <div className="relative flex items-center justify-center">
            <div
                className={`flex items-center justify-center rounded-full cursor-pointer p-2 transition-transform duration-300 ease-in-out ${typeClasses[type]} ${className}`}
                onClick={() => setOpen(!open)}
            >
                <Info className="text-white" />
            </div>
            <div className={`absolute inset-0 flex items-center justify-center transition-opacity duration-300 ${open ? "opacity-100 scale-100 pointer-events-auto" : "opacity-0 scale-90 pointer-events-none"}`} onClick={() => setOpen(false)}>
                {icons.map((item, index) => (
                    <div
                        key={index}
                        className="absolute transition-all duration-300 ease-in-out flex flex-col items-center"
                        style={{
                            transform: open ? "translateY(0)" : "translateY(5px)",
                            opacity: open ? 1 : 0,
                            top: index === 0 ? "-30px" : "auto",
                            left: index === 1 ? "-50px" : "auto",
                            right: index === 2 ? "-50px" : "auto"
                        }}
                    >
                        {item.icon}
                        <span className="text-xs text-white bg-gray-700 px-2 py-1 rounded opacity-0 group-hover:opacity-100 transition-opacity duration-300">{item.label}</span>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Tooltip;