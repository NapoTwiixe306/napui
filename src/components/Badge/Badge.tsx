export interface BadgeProps {
    children: React.ReactNode
    icon?: React.ReactNode
    theme?: "light" | "dark"
    type?: "basic" | "Failed" | "Success" | "Pending"
    doted?: boolean
    dotColor?: string
    className?: string
}

const Badge: React.FC<BadgeProps> = ({ 
    children, 
    icon, 
    theme = "light", 
    type = "basic", 
    doted = false,
    dotColor,
    className = ""
}) => {
    const typeClasses: Record<string, string> = {
        Failed: "bg-red-500 text-white",
        Success: "bg-green-500 text-white",
        Pending: "bg-orange-500 text-white",
        basic: theme === "light" ? "text-black bg-gray-200" : "text-white bg-gray-800"
    };

    const resolvedDotColor = dotColor || (typeClasses[type]?.split(" ")[0] || "bg-gray-500");

    return (
        <div className={`flex items-center gap-2 px-3 py-1 rounded-full ${typeClasses[type] || ""} ${className}`}>
            {doted && <span className={`h-2 w-2 ${resolvedDotColor} rounded-full`} />}
            {children}
            {icon}
        </div>
    )
}

export default Badge;
