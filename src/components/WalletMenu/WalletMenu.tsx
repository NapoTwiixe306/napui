
import { useState } from "react";
import { motion } from "framer-motion";
import { Clipboard, Wallet, Info } from "lucide-react";
import { Button } from "../Button";

interface WalletMenuProps {
  walletAddress?: string | null;
  label?: string;
  onCopy: () => void;
  onChangeWallet: () => void;
}

const WalletMenu: React.FC<WalletMenuProps> = ({
  walletAddress,
  label,
  onCopy,
  onChangeWallet,
}) => {
  const [isOpen, setIsOpen] = useState(false);

  const formattedAddress =
    walletAddress && walletAddress.length > 10
      ? `${walletAddress.slice(0, 6)}...${walletAddress.slice(-4)}`
      : "Non connecté";

  return (
    <div className="relative">
      <Button
        onClick={() => setIsOpen(!isOpen)}
        type="basic"
        className="bg-gray-800 text-white px-4 py-2 rounded-lg flex items-center gap-2"
        size="sm"
      >
        {label || formattedAddress} 
        <Info />
      </Button>

      {isOpen && (
        <motion.div
          initial={{ opacity: 0, scale: 0.95 }}
          animate={{ opacity: 1, scale: 1 }}
          exit={{ opacity: 0, scale: 0.95 }}
          className="absolute mt-2 w-48 bg-gray-900 text-white rounded-lg shadow-lg px-3 py-4 flex flex-col gap-2"
        >
          <p className="text-sm text-gray-400">{formattedAddress}</p>
          <Button
            onClick={onCopy}
            size="sm"
            type="basic"
            className="flex items-center gap-2 w-full px-4 py-2 hover:bg-gray-700 rounded"
          >
            <Clipboard size={16} />
            Copier l'adresse
          </Button>
          <Button
            size="sm"
            type="basic"
            onClick={onChangeWallet}
            className="flex items-center gap-2 w-full px-4 py-2 hover:bg-gray-700 rounded"
          >
            <Wallet size={16} />
            Changer de portefeuille
          </Button>
        </motion.div>
      )}
    </div>
  );
};

export default WalletMenu;
