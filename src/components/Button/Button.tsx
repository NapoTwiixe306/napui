import { useState, useEffect } from 'react';
import { OctagonX, Check, CircleAlert, Wallet, LogOut } from 'lucide-react';

export interface ButtonProps {
    size: "sm" | "md" | "lg" | "xl";
    type: "basic" | "warning" | "error" | "success" | "wallet";
    children?: React.ReactNode;
    icon?: React.ReactNode;
    onClick?: () => void;
    className?: string;
}

const Button: React.FC<ButtonProps> = ({ size, children, icon, type, onClick, className = "" }) => {
    const [isConnected, setIsConnected] = useState(false);
    const [account, setAccount] = useState<string | null>(null);

    useEffect(() => {
        const checkWalletConnection = async () => {
            if (window.ethereum) {
                try {
                    const accounts = await window.ethereum.request({ method: 'eth_accounts' }) as string[];
                    if (accounts.length > 0) {
                        setAccount(accounts[0]);
                        setIsConnected(true);
                    }
                } catch (error) {
                    console.error("Failed to check wallet connection:", error);
                }
            }
        };

        checkWalletConnection();
    }, []);

    const sizeClasses = {
        sm: "w-auto px-2 h-[38px] flex items-center justify-center text-sm",
        md: "w-auto px-2 h-[40px] flex items-center justify-center text-md",
        lg: "w-auto px-2 h-[44px] flex items-center justify-center text-lg",
        xl: "w-auto px-2 h-[48px] flex items-center justify-center text-xl"
    };

    const typeClasses = {
        basic: "bg-blue-500",
        warning: "bg-orange-500",
        error: "bg-red-500",
        success: "bg-green-500",
        wallet: "bg-yellow-500" 
    };

    const defaultIcon: { [key in "warning" | "error" | "success" | "basic" | "wallet"]: React.ReactNode } = {
        basic: <span className=""></span>,
        warning: <span className="mr-2 text-xl"><CircleAlert /></span>,
        error: <span className="mr-2 text-xl"><OctagonX /></span>,
        success: <span className="mr-2 text-xl"><Check /></span>,
        wallet: isConnected ? <LogOut className="w-4 h-4 ml-2" /> : <Wallet className="w-4 h-4 ml-2" />
    };

    const iconToDisplay = icon || (type !== "basic" ? defaultIcon[type] : null);

    const connectWallet = async () => {
        if (window.ethereum) {
            try {
                const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' }) as string[];
                setAccount(accounts[0]);
                setIsConnected(true);
            } catch (error) {
                console.error("Failed to connect wallet:", error);
            }
        } else {
            alert('Please install MetaMask or another Ethereum wallet provider.');
        }
    };

    const disconnectWallet = () => {
        setAccount(null);
        setIsConnected(false);
    };

    const handleButtonClick = () => {
        if (type === "wallet") {
            if (isConnected) {
                disconnectWallet();
            } else {
                connectWallet();
            }
        } else if (onClick) {
            onClick();
        }
    };

    const backgroundClass = className.includes('bg-') ? className : typeClasses[type];

    return (
        <button 
            className={`cursor-pointer text-white rounded flex flex-row items-center justify-center gap-2 ${sizeClasses[size]} ${backgroundClass}`} 
            onClick={handleButtonClick}
        >
            {type === "wallet" && isConnected ? (
                <>
                    <span className="text-sm">{account?.slice(0, 6)}...{account?.slice(-4)}</span>
                    {iconToDisplay}
                </>
            ) : (
                <>
                    {iconToDisplay}
                    {children}
                </>
            )}
        </button>
    );
    
};

export default Button;
