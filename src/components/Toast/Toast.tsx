import { useEffect, useState } from "react";
import { motion, AnimatePresence } from "framer-motion";

export interface ToastProps {
  title: string;
  description: string;
  type: "Success" | "Failed" | "Warning";
  className?: string;
  onClose?: () => void;
}

const Toast: React.FC<ToastProps> = ({ title, description, type, className = "", onClose }) => {
  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsVisible(false);
      if (onClose) {
        onClose();
      }
    }, 3000); 

    return () => clearTimeout(timer);
  }, [onClose]);

  const typeClasses = {
    Success: "bg-green-100 border-l-4 border-green-500 text-green-700",
    Failed: "bg-red-100 border-l-4 border-red-500 text-red-700",
    Warning: "bg-orange-100 border-l-4 border-orange-500 text-orange-700",
  };

  return (
    <AnimatePresence>
      {isVisible && (
        <motion.div
          initial={{ opacity: 0, x: 50 }}
          animate={{ opacity: 1, x: 0 }}
          exit={{ opacity: 0, x: 50 }}
          transition={{ duration: 0.3 }}
          className={`${className} ${typeClasses[type]} fixed top-5 right-5 p-4 rounded shadow-md w-80 flex items-start`}
        >
          <div className="flex-1">
            <h1 className="font-bold">{title}</h1>
            <p className="text-sm">{description}</p>
          </div>
          <button onClick={() => setIsVisible(false)} className="ml-4 text-gray-500 hover:text-gray-700">
            ✕
          </button>
        </motion.div>
      )}
    </AnimatePresence>
  );
};

export default Toast;
