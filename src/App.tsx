/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect } from 'react';
import { Badge, Button, Toast, Tooltip, WalletMenu } from './components'
import {Bitcoin, Wallet, Globe, Shield, GitBranch} from 'lucide-react'
import Web3 from "web3";
export default function home(){
    const [showToast, setShowToast] = useState(false);
    const [walletAddress, setWalletAddress] = useState<string | null>(null);

  useEffect(() => {
    const loadWallet = async () => {
      if (window.ethereum) {
        const web3 = new Web3(window.ethereum);
        try {
          const accounts = await web3.eth.getAccounts();
          if (accounts.length > 0) {
            setWalletAddress(accounts[0]);
          }
        } catch (error) {
          console.error("Erreur lors de la récupération du compte :", error);
        }
      }
    };
    loadWallet();
  }, []);

  // 📌 Fonction pour copier l'adresse
  const handleCopyAddress = () => {
    if (walletAddress) {
      navigator.clipboard.writeText(walletAddress);
      alert("Adresse copiée : " + walletAddress);
    } else {
      alert("Aucune adresse trouvée.");
    }
  };

  // 📌 Fonction pour changer de portefeuille
  const handleChangeWallet = async () => {
    if (window.ethereum) {
      try {
        const accounts = await window.ethereum.request({
          method: "eth_requestAccounts",
        }) as string[];
        setWalletAddress(accounts[0]); // Met à jour l'adresse affichée
        alert("Portefeuille changé : " + accounts[0]);
      } catch (error) {
        console.error("Erreur lors du changement de portefeuille :", error);
        alert("Changement de portefeuille annulé.");
      }
    } else {
      alert("Aucun fournisseur Web3 détecté.");
    }
  };
    function handleClick() {
        alert("yop")
    }
    return(
        <div className="p-6 bg-black h-screen">
            <div className="flex items-center gap-6">
                <Button size="sm" type="wallet" className='bg-blue-500' onClick={handleClick}>Connect Wallet</Button>
                <Button size="md" type="warning">Warning Button</Button>
                <Button size="lg" type="wallet">Connect Wallet</Button>
                <Button size="xl" type="success" icon={<Bitcoin className="h-5 w-5"/>} onClick={handleClick}>Success Button</Button>
            </div>
            <div className="flex items-center gap-6 mt-10">
                <Badge theme='dark' type='Pending' doted dotColor='bg-orange-900'>Test1</Badge>
                <Badge theme='dark' type='Success' doted dotColor='bg-green-900'>Test2</Badge>
                <Badge theme='dark' type='Failed' doted dotColor='bg-brand-100'>Test3</Badge>
                <Badge theme='light' type='basic'>Test</Badge>
            </div>
            <div className="flex items-center gap-6 mt-10">
                <button onClick={() => setShowToast(true)} className="px-4 py-2 bg-blue-500 text-white rounded">
                    Afficher le Success
                </button>

                {showToast && (
                    <Toast
                    title="Succès"
                    description="Votre action a été enregistrée avec succès."
                    type="Success"
                    onClose={() => setShowToast(false)}
                    />
                )}
            </div>
            <div className="flex items-center gap-32 mt-10 ml-4">
                <Tooltip
                    type="sm"
                    icons={[
                        { icon: <Wallet className="text-white" />, label: "Portefeuille" },
                        { icon: <Globe className="text-white" />, label: "Global" },
                        { icon: <Shield className="text-white" />, label: "Sécurité" }
                    ]}
                />
                <Tooltip 
                    type='md'
                    icons={[
                        { icon: <Wallet className="text-white" />, label: "Portefeuille" },
                        { icon: <Globe className="text-white" />, label: "Global" },
                        { icon: <GitBranch className="text-white" />, label: "Sécurité" }
                    ]}
                />
                <Tooltip 
                    type='lg'
                    icons={[
                        { icon: <Wallet className="text-white" />, label: "Portefeuille" },
                        { icon: <Globe className="text-white" />, label: "Global" },
                        { icon: <Shield className="text-white" />, label: "Sécurité" }
                    ]}
                />
            </div>
            <div className="flex items-center gap-6 mt-10">
                <WalletMenu
                    walletAddress={walletAddress}
                    onCopy={handleCopyAddress}
                    onChangeWallet={handleChangeWallet}
                />
            </div>
        </div>
    )
}