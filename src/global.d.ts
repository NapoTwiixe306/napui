// src/global.d.ts
interface Ethereum {
    request: (args: { method: string; params?: unknown[] }) => Promise<unknown>;
    // Vous pouvez ajouter d'autres méthodes si nécessaire comme `enable`, `on`, etc.
  }
  
  interface Window {
    ethereum?: Ethereum;
  }
  